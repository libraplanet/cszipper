﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsZipper.Zip.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class Crc32
    {
        static uint[] tbl;

        /// <summary>
        /// /
        /// </summary>
        static Crc32()
        {
            //create tbl
            {
                const uint POLY = 0xEDB88320;
                tbl = new uint[256];

                for (int i = 0; i < tbl.Length; i++)
                {
                    uint u = (uint)i;
                    for (int j = 0; j < 8; j++)
                    {
                        if ((u & 0x01) != 0)
                        {
                            u = (u >> 1) ^ POLY;
                        }
                        else
                        {
                            u = (u >> 1);
                        }
                    }
                    tbl[i] = (uint)(u & 0xFFFFFFFF);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static uint GetCRC(byte[] d)
        {
            uint res = 0xFFFFFFFF;
            for (int i = 0; i < d.Length; i++)
            {
                //result = (result >> 8) ^ table[buffer[i] ^ (result & 0xFF)];
                long low = ((long)(res) & 0xFF);
                long high = ((long)(res) >> 8) & 0xFFFFFFFF;
                long t = ((long)tbl[d[i] ^ low]);
                res = (uint)((high ^ t) & 0xFFFFFFFF);
            }
            return ~res;
        }
    }
}
