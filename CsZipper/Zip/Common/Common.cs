﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsZipper.Zip.Common
{
    static class Common
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <returns></returns>
        public static short GetDosTime(int hour, int min, int sec)
        {
            //opt
            {
                hour %= 24;
                min %= 60;
                sec %= 60;
            }
            //ret
            {
                short ret = 0;
                ret |= (short)(hour << 11);
                ret |= (short)(min << 5);
                ret |= (short)(sec >> 1);
                return ret;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public static short GetDosDate(int year, int month, int day)
        {
            //opt
            {
                month %= 12;
                day %= 32;
                year -= 1980;
                //month++;
            }
            //ret
            {
                short ret = 0;
                ret |= (short)(year << 9);
                ret |= (short)(month << 5);
                ret |= (short)(day);
                return ret;
            }
        }
    }
}
