﻿using System;
using System.IO;
using System.Text;

namespace CsZipper.Zip.Common
{
    public class ZipData
    {
        /// <summary>
        /// 
        /// </summary>
        public class PathSet
        {
            public readonly string dataPath;
            public readonly string zipPath;
            public PathSet(string dataPath, string zipPath)
            {
                this.dataPath = dataPath;
                this.zipPath = zipPath;
            }
        }

        Encoding enc = Encoding.Default;

        PathSet pathSet;
        bool bFile;

        //unsigned int headerpos;
        uint positioin;
        //unsigned short needver;
        short needver = 10;
        //unsigned short option;
        short option = 0;
        //unsigned short compression method;
        short comptype;
        //unsigned short filetime;
        DateTime timestamp;
        //unsigned int crc32;
        uint crc32;
        //unsigned int compsize;  
        uint compsize;
        //unsigned int uncompsize;
        uint uncompsize;
        //unsigned short fnamelen;
        //short fnamelen;
        //unsigned short extralen;  
        short extralen = 0;
        //unsigned short commentlen;  
        short commentlen = 0;
        //unsigned short disknum;
        short disknum = 0;
        //unsigned short inattr;
        short inattr;
        //unsigned int outattr;
        uint outattr;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathSet"></param>
        /// <param name="bFile"></param>
        public ZipData(PathSet pathSet, bool bFile)
        {
            this.pathSet = pathSet;
            this.bFile = bFile;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private short GetDosTime()
        {
            return Common.GetDosTime(timestamp.Hour, timestamp.Minute, timestamp.Second);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private short GetDosDate()
        {
            return Common.GetDosDate(timestamp.Year, timestamp.Month, timestamp.Day);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private short GetFnameLength()
        {
            return (short)enc.GetByteCount(this.pathSet.zipPath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private byte[] GetFnameData()
        {
            return enc.GetBytes(this.pathSet.zipPath);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="bWriter"></param>
        public void writeFileEntry(BinaryWriter bWriter)
        {
            byte[] bin;
            //collect
            {
                FileInfo fileInfo = new FileInfo(this.pathSet.dataPath);
                timestamp = fileInfo.LastWriteTime;
                positioin = (uint)bWriter.BaseStream.Position;

                if (bFile)
                {
                    //bin = File.ReadAllBytes(this.pathSet.dataPath);
                    byte[] d = File.ReadAllBytes(this.pathSet.dataPath);

                    Compless.Compless cmp = new Compless.Plane(d);
                    //Compless.Compless cmp = new Compless.Deflate(d);

                    bin = cmp.Data;
                    comptype = cmp.CompressionMethod;
                    compsize = cmp.CompressionSize;
                    uncompsize = cmp.UnCompressionSize;

                    crc32 = Crc32.GetCRC(d);
                    inattr = 0;
                    outattr = (uint)0x01;
                }
                else
                {
                    bin = new byte[0];
                    compsize = (uint)0;
                    uncompsize = (uint)0;
                    crc32 = (uint)0;
                    inattr = 0;
                    outattr = (uint)0x10;
                }
            }

            //write
            {
                //headers
                {
                    //unsigned int signature;
                    bWriter.Write(Signature.FILE_HEADER);
                    //unsigned short needver;  
                    bWriter.Write(this.needver);
                    //unsigned short option;  
                    bWriter.Write(this.option);
                    //unsigned short comptype;  
                    bWriter.Write(this.comptype);
                    //unsigned short filetime;
                    bWriter.Write(GetDosTime());
                    //unsigned short filedate;
                    bWriter.Write(GetDosDate());
                    //unsigned int crc32;
                    bWriter.Write(this.crc32);
                    //unsigned int compsize;  
                    bWriter.Write(this.compsize);
                    //unsigned int uncompsize;
                    bWriter.Write(this.uncompsize);
                    //unsigned short fnamelen;
                    bWriter.Write(GetFnameLength());
                    //unsigned short extralen;
                    bWriter.Write(this.extralen);
                }
                //datas
                {
                    //fname
                    bWriter.Write(GetFnameData());

                    //extra
                    {
                    }

                    //data
                    bWriter.Write(bin);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bWriter"></param>
        public void writeCentralDirectryHeader(BinaryWriter bWriter)
        {
            //header
            {
                //unsigned int signature;
                bWriter.Write(Signature.CENTRAL_DIRECTORY_HEADER);
                //unsigned short madever
                bWriter.Write((short)10);
                //unsigned short needver;
                bWriter.Write(this.needver);
                //unsigned short option;
                bWriter.Write(this.option);
                //unsigned short comptype;
                bWriter.Write(this.comptype);
                //unsigned short filetime;
                bWriter.Write(GetDosTime());
                //unsigned short filedate;
                bWriter.Write(GetDosDate());
                //unsigned int crc32;
                bWriter.Write(this.crc32);
                //unsigned int compsize;  
                bWriter.Write(this.compsize);
                //unsigned int uncompsize;  
                bWriter.Write(this.uncompsize);
                //unsigned short fnamelen;
                bWriter.Write(GetFnameLength());
                //unsigned short extralen;  
                bWriter.Write(this.extralen);
                //unsigned short commentlen;  
                bWriter.Write(this.commentlen);
                //unsigned short disknum;  
                bWriter.Write(this.disknum);
                //unsigned short inattr;  
                bWriter.Write(this.inattr);
                //unsigned int outattr;  
                bWriter.Write(this.outattr);
                //unsigned int headerpos;
                bWriter.Write(this.positioin);
            }

            //data
            {
                bWriter.Write(GetFnameData());
            }
        }
    }
}
