﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsZipper.Zip.Common
{
    public static class Signature
    {
        public static readonly uint FILE_HEADER = (uint)0x04034B50;
        public static readonly uint CENTRAL_DIRECTORY_HEADER = (uint)0x02014b50;
        public static readonly uint END_OF_CENTRAL_DIRECTORY_HEADER = (uint)0x06054B50;
    }
}
