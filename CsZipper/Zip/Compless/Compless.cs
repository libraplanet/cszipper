﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsZipper.Zip.Compless
{
    public abstract class Compless
    {
        public static readonly short PLANE = 0;
        public static readonly short DEFLATE = 8;

        public readonly byte[] Data;
        public readonly short CompressionMethod;
        public readonly uint CompressionSize;
        public readonly uint UnCompressionSize;

        protected Compless(short comp, byte[] data)
        {
            CompressionMethod = comp;
            Data = GetComplessedData(data);
            CompressionSize = (uint)Data.Length;
            UnCompressionSize = (uint)data.Length;
        }

        protected abstract byte[] GetComplessedData(byte[] data);
    }
}
