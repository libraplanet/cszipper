﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsZipper.Zip.Compless
{
    class Plane : Compless
    {
        public Plane(byte[] data)
            : base(Compless.PLANE, data)
        {
        }

        protected override byte[] GetComplessedData(byte[] data)
        {
            byte[] t = new byte[data.Length];
            Array.Copy(data, t, data.Length);
            return t;
        }
    }
}
