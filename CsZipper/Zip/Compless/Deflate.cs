﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.IO.Compression;

namespace CsZipper.Zip.Compless
{
    class Deflate : Compless
    {
        public Deflate(byte[] data)
            : base(Compless.DEFLATE, data)
        {
        }

        protected override byte[] GetComplessedData(byte[] data)
        {
            using (MemoryStream mStream = new MemoryStream())
            {
                using (DeflateStream dStream = new DeflateStream(mStream, CompressionMode.Compress))
                {
                    dStream.Write(data, 0, data.Length);
                }
                return mStream.ToArray();
            }

            //throw new NotImplementedException();
            //return new byte[0];
        }
    }
}
