﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using CsZipper.Zip.Common;

namespace CsZipper.Zip
{
    class ZipContainerStream : IDisposable
    {
        List<ZipData.PathSet> listFileEntry = new List<ZipData.PathSet>();
        List<ZipData.PathSet> listDirEntry = new List<ZipData.PathSet>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataPath"></param>
        /// <param name="zipPath"></param>
        public void AddFileEntry(string dataPath, string zipPath)
        {
            listFileEntry.Add(new ZipData.PathSet(dataPath, zipPath));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataPath"></param>
        /// <param name="zipPath"></param>
        public void AddDirectryEntry(string dataPath, string zipPath)
        {
            listDirEntry.Add(new ZipData.PathSet(dataPath, zipPath));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fname"></param>
        public void Save(string fname)
        {
            List<ZipData> listZipData = new List<ZipData>();

            //collection
            {
                foreach (ZipData.PathSet set in listFileEntry)
                {
                    listZipData.Add(new ZipData(set, true));
                }

                foreach (ZipData.PathSet set in listDirEntry)
                {
                    listZipData.Add(new ZipData(set, false));
                }
            }

            using (FileStream fStream = File.Create(fname))
            {
                using (BinaryWriter bWriter = new BinaryWriter(fStream))
                {
                    long dhPos, dhSize;
                    //[file entry]
                    {
                        foreach (ZipData data in listZipData)
                        {
                            data.writeFileEntry(bWriter);
                        }
                    }

                    //[central directry header]
                    {
                        dhPos = bWriter.BaseStream.Position;
                        foreach (ZipData data in listZipData)
                        {
                            data.writeCentralDirectryHeader(bWriter);
                        }
                        dhSize = bWriter.BaseStream.Position - dhPos;
                    }

                    //[end directry header]
                    {
                        //unsigned int signature;
                        bWriter.Write((uint)0x06054B50);
                        //unsigned short disknum;
                        bWriter.Write((short)0);
                        //unsigned short startdisknum;
                        bWriter.Write((short)0);
                        //unsigned short diskdirentry;
                        bWriter.Write((short)listZipData.Count);
                        //unsigned short direntry;
                        bWriter.Write((short)listZipData.Count);
                        //unsigned int dirsize;
                        bWriter.Write((uint)dhSize);
                        //unsigned int startpos;
                        bWriter.Write((uint)dhPos);
                        //unsigned short commentlen;
                        bWriter.Write((short)0);
                    }
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
