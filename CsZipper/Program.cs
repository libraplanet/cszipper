﻿using System;
using System.IO;
using System.Collections;
using System.Text;
using CsZipper.Zip;

namespace CsZipper
{
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orgPath"></param>
        /// <param name="basePath"></param>
        /// <param name="isFile"></param>
        /// <returns></returns>
        static string GetZipPath(string orgPath, string basePath, bool isFile)
        {
            string s = orgPath;
            s = s.Substring(basePath.Length);
            while (s.StartsWith(@"\"))
            {
                s = s.Substring(1);
            }
            if (!isFile && !s.EndsWith(@"\"))
            {
                s += @"\";
            }
            s = s.Replace('\\', '/');
            return s;
        }

        static void Main(string[] args)
        {
            try
            {
                //init
                {
                    Console.WriteLine("//----------------------------------------");
                    Console.WriteLine("//     CsZipper");
                    Console.WriteLine("//        auther: takumi");
                    Console.WriteLine("//        date  : 110529");
                    Console.WriteLine("//----------------------------------------");
                    Console.WriteLine("");
                }

                if ((args == null)
                || (args.Length < 1)
                || ("/h" == args[0])
                || ("/help" == args[0]))
                {
                    Console.WriteLine("[help]");
                    Console.WriteLine("");
                    Console.WriteLine("usage :");
                    Console.WriteLine("  [pack zip.]");
                    Console.WriteLine("    CsZipper /p [output file name] [input dir]");
                    Console.WriteLine("    CsZipper /pack [output file name] [input dir]");
                    Console.WriteLine("");
                    Console.WriteLine("  [pack zip.]");
                    Console.WriteLine("    CsZipper /u [output dir] [input file name]");
                    Console.WriteLine("    CsZipper /unpack [output dir] [input file name]");
                    Console.WriteLine("");
                    Console.WriteLine("  [help]");
                    Console.WriteLine("    CsZipper /h");
                    Console.WriteLine("    CsZipper /help");

                }
                else if ("/pack" == args[0])
                {
                    Console.WriteLine("[pack]");
                    pack(args[1], args[2]);
                }
                else if ("/unpack" == args[0])
                {
                    Console.WriteLine("[unpack]");
                    unpack(args[1], args[2]);
                }               
            }
            catch(Exception e)
            {
                Console.WriteLine("appear exception!");
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strOutFileName"></param>
        /// <param name="strInDirName"></param>
        static void pack(string strOutFileName, string strInDirName)
        {
            //arg
            {
                Console.WriteLine("[collect]");
                Console.WriteLine("  strOutFileName = " + strOutFileName);
                Console.WriteLine("    (" + Path.GetFullPath(strOutFileName) + ")");
                Console.WriteLine("  strInDirName = " + strInDirName);
                Console.WriteLine("    (" + Path.GetFullPath(strInDirName) + ")");
                Console.WriteLine();
            }

            //collection
            {
                ZipContainerStream zc = new ZipContainerStream();
                Action<string> lambda = null;
                lambda = delegate(string dir)
                {
                    //collect file
                    {
                        string[] files = Directory.GetFiles(dir);
                        foreach (string f in files)
                        {
                            zc.AddFileEntry(f, GetZipPath(f, strInDirName, true));
                        }
                    }
                    //collect dir
                    {
                        string[] dirs = Directory.GetDirectories(dir);
                        foreach (string d in dirs)
                        {
                            zc.AddDirectryEntry(d, GetZipPath(d, strInDirName, false));
                            lambda(d);
                        }
                    }
                };

                Console.WriteLine("[run]");
                lambda(strInDirName);
                zc.Save(strOutFileName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strInFileName"></param>
        /// <param name="strOutDirName"></param>
        static void unpack(string strOutDirName, string strInFileName)
        {
            //arg
            {
                Console.WriteLine("[collect]");
                Console.WriteLine("  strInFileName = " + strInFileName);
                Console.WriteLine("    (" + Path.GetFullPath(strInFileName) + ")");
                Console.WriteLine("  strOutDirName = " + strOutDirName);
                Console.WriteLine("    (" + Path.GetFullPath(strOutDirName) + ")");
                Console.WriteLine();
            }

            //collection
            {
                using (FileStream fs = new FileStream(strInFileName, FileMode.Open, FileAccess.Read))
                {
                    UnZipCollector uz = new UnZipCollector();
                    uz.Collection(fs);
                    uz.Save(strOutDirName);
                }
            }
        }
    }
}
